package com.bustamante.rssreader.SAXParser;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import com.bustamante.rssreader.models.RSS_Channel;

public class RSS_SAX_Parser {

	 private URL rssUrl;
	 
    public RSS_SAX_Parser(String url)
    {
        try
        {
            this.rssUrl = new URL(url);
        }
        catch (MalformedURLException e)
        {
            throw new RuntimeException(e);
        }
    }
 
    public RSS_Channel parse()
    {
        SAXParserFactory factory = SAXParserFactory.newInstance();
 
        try
        {
            SAXParser parser = factory.newSAXParser();
            RSS_SAX_Handler handler = new RSS_SAX_Handler();
            parser.parse(this.getInputStream(), handler);
            return handler.getRSS_Channel();
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }
 
    private InputStream getInputStream()
    {
        try
        {
            return rssUrl.openConnection().getInputStream();
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }
	
}
