package com.bustamante.rssreader.SAXParser;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.bustamante.rssreader.models.RSS_Channel;
import com.bustamante.rssreader.models.RSS_Item;

public class RSS_SAX_Handler extends DefaultHandler {
	
	    private RSS_Channel rss_channel;
	    private RSS_Item rss_item;
	    private StringBuilder sbText;
	    
	    private Boolean channel_image_parsing = false;
	 
	    public RSS_Channel getRSS_Channel(){
	        return rss_channel;
	    }
	 
	    @Override
	    public void characters(char[] ch, int start, int length) throws SAXException {
	 
	        super.characters(ch, start, length);
	 
	        sbText.append(ch, start, length);
	    }
	 
	    @Override
	    public void endElement(String uri, String localName, String name) throws SAXException {
	 
	        super.endElement(uri, localName, name);
	 
	        if (this.rss_channel!=null && this.rss_item != null) {
	 	        	
	        	if (localName.equals("title")) {
	        		this.rss_item.setTitle(sbText.toString().trim());
	            } else if (localName.equals("link")) {
	            	this.rss_item.setLink(sbText.toString().trim());
	            } else if (localName.equals("description")) {
	            	this.rss_item.setDescription(sbText.toString().trim());
	            } else if (localName.equals("guid")) {
	            	this.rss_item.setGuid(sbText.toString().trim());
	            } else if (localName.equals("pubDate")) {
	            	this.rss_item.setPub_date(sbText.toString().trim());
	            } else if (localName.equals("item")) {
	                rss_channel.addItem(this.rss_item);
	                this.rss_item = null;
	            }
		        this.sbText = new StringBuilder();
	        }
	        else if (this.rss_channel!=null)
	        {
	        	if (localName.equals("title")) {
	        		this.rss_channel.setTitle(sbText.toString().trim());
	            } else if (localName.equals("link")) {
	            	this.rss_channel.setLink(sbText.toString().trim());
	            } else if (localName.equals("description")) {
	            	this.rss_channel.setDescription(sbText.toString().trim());
	            } else if (localName.equals("url") && this.channel_image_parsing) {
	            	this.rss_channel.setImage_url(sbText.toString().trim());
	            	this.channel_image_parsing = false;
	            }
		        this.sbText = new StringBuilder();
	        }
	        
	    }
	 
	    @Override
	    public void startDocument() throws SAXException {
	 
	        super.startDocument();
	 	        
	        this.sbText = new StringBuilder();
	    }
	 
	    @Override
	    public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException {
	 
	        super.startElement(uri, localName, name, attributes);
	 
	        if (localName.equals("channel")) {
	        	this.rss_channel = new RSS_Channel();
	        }
	        else if (this.rss_channel != null && localName.equals("item")) {
	        	this.rss_item = new RSS_Item();
	        }
	        else if (this.rss_item != null && localName.equals("enclosure")) {
	        	if (((String)attributes.getValue("type")).startsWith("image"))
	        	{
	        		this.rss_item.setImage_url(((String)attributes.getValue("url")).trim());
	        	}
	        }
	    }
	
}
