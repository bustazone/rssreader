package com.bustamante.rssreader.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;

import android.content.Context;

public class SerializableUtils {
	
	public static void deleteFile(Context context, String fileName){

		if (existFile(context, fileName))
		{
			context.deleteFile(fileName);
		}	
	}	
	
	public static void serialize(Context context, String fileName, Object values){
		FileOutputStream fosRecordType;
		try {
			fosRecordType = context.openFileOutput(fileName, Context.MODE_PRIVATE);
			ObjectOutputStream os;
			os = new ObjectOutputStream(fosRecordType);
			os.writeObject(values);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}catch (IOException e) {
			e.printStackTrace();
		}
	}


	public static String deserialize(Context context, String fileName){
		String ret = null;
		try {
			FileInputStream fis = context.openFileInput(fileName);
			StringBuffer fileContent = new StringBuffer("");
			
			BufferedReader in = new BufferedReader(new InputStreamReader(fis,"UTF-8"));
			String readLine;

			while ((readLine = in.readLine()) != null) {
				
				fileContent.append(readLine);
			}			
			
			ret = fileContent.toString();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (StreamCorruptedException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return ret;
	}

	public static boolean existFile(Context context, String fileName){
		File file;
		file = context.getFileStreamPath(fileName);
		return file.exists();
	}
}
