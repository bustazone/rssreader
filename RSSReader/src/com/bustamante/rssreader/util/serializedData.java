package com.bustamante.rssreader.util;

import java.util.Collections;
import java.util.Comparator;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.bustamante.rssreader.R;
import com.bustamante.rssreader.SAXParser.RSS_Url_Load_Async;
import com.bustamante.rssreader.interfaces.AsyncRequestCallbacks;
import com.bustamante.rssreader.models.RSS_Channel;
import com.bustamante.rssreader.models.RSS_Item;
import com.google.gson.Gson;

public class serializedData implements AsyncRequestCallbacks{

	private AsyncRequestCallbacks mCallbacks;
	private Context mCtx;
	
	public Boolean isConnected (Context ctx)
	{
		 ConnectivityManager connectivityManager = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
	     NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	     return activeNetworkInfo != null; 
	}
	
	public RSS_Channel getChannel (String Url, Context ctx, AsyncRequestCallbacks callback, Boolean forcedUpdate)
	{
		mCallbacks = callback;
		mCtx = ctx;
			
		//Si no hay conexi�n se muestra un alert con el aviso
		if (!isConnected(ctx))
		{
			mCallbacks.onError(mCtx.getString(R.string.connection_problems));
		}
		
		//Si no hay conexi�n o no se ha forzado la descarga y adem�s existe un serializado anterior se devuelve dicho serializado.
		if ((!isConnected(ctx) || !forcedUpdate) && SerializableUtils.existFile(ctx, ctx.getString(R.string.filename_serialized_channel)))
		{			
			String recs = (String) SerializableUtils.deserialize(ctx, ctx.getString(R.string.filename_serialized_channel));

			if (recs.indexOf("{")>=0)
			{
				return (RSS_Channel) new Gson().fromJson(recs.substring(recs.indexOf("{")), new com.google.gson.reflect.TypeToken<RSS_Channel>(){}.getType());
			}
		}
		//En otro caso (se ha forzado la descarga o no existe un serializado anterior), si hay conexi�n se descarga
		else if (isConnected(ctx))
		{						
			new RSS_Url_Load_Async(Url, this).execute();
		}	
	
		//Si se ha hecho la petici�n o no se ha encontrado un serializado, se devuelve un channel vacio.
		return new RSS_Channel();
	}

	@Override
	public void onSuccess(RSS_Channel channel) {
		Collections.sort(channel.getItems(), new Comparator<RSS_Item>() {
			public int compare(RSS_Item o1, RSS_Item o2) {
		    	return o1.getPub_dateParsed().compareTo(o2.getPub_dateParsed());
			}
		});
		//Serializamos la devoluci�n con formato json
		String jso = new Gson().toJson(channel, new com.google.gson.reflect.TypeToken<RSS_Channel>(){}.getType());
		SerializableUtils.serialize(mCtx, mCtx.getString(R.string.filename_serialized_channel), jso);
		mCallbacks.onSuccess(channel);
	}

	@Override
	public void onError(String errorMsg) {
		Log.e("Request Problems", "mError_Msg");
		mCallbacks.onError(mCtx.getString(R.string.request_problems));
	}
}
