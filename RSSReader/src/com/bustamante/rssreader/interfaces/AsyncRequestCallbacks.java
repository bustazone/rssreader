package com.bustamante.rssreader.interfaces;

import com.bustamante.rssreader.models.RSS_Channel;

public interface AsyncRequestCallbacks {
	public void onSuccess (RSS_Channel channel);
	public void onError (String errorMsg);
}
