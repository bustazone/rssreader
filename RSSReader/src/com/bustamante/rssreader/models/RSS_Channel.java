package com.bustamante.rssreader.models;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

public class RSS_Channel {

	@Expose
	private String title;
	@Expose
    private String link;
	@Expose
    private String description;
	@Expose
    private String image_url;
	@Expose
    private List<RSS_Item> items;
    
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getImage_url() {
		return image_url;
	}
	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}
	public List<RSS_Item> getItems() {
		return items;
	}
	public void setItems(List<RSS_Item> items) {
		this.items = items;
	}
	public void addItem(RSS_Item item) {
		if (this.items==null)
		{
			this.items = new ArrayList<RSS_Item>();
		}
		this.items.add(item);
	}
  
}
