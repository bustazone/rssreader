package com.bustamante.rssreader.models;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.google.gson.annotations.Expose;

public class RSS_Item {

	public static DateFormat RSSDateFormatter = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z",Locale.US);
	
	@Expose
	private String title;
	@Expose
	private String link;
	@Expose
	private String description;
	@Expose
	private String guid;
	@Expose
	private String pub_date;
	@Expose
	private String image_url;
    
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
	public String getPub_date() {
		return pub_date;
	}
	public Date getPub_dateParsed() {
		try {
			return RSSDateFormatter.parse(pub_date);
		} catch (ParseException e) {
			return null;
		}
	}
	public void setPub_date(String pub_date) {
		this.pub_date = pub_date;
	}
	public String getImage_url() {
		return image_url;
	}
	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}   
	
}
