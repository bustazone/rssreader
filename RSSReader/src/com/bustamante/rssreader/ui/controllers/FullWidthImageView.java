package com.bustamante.rssreader.ui.controllers;

import com.bustamante.rssreader.ui.imageLoader.NetworkImageViewCustom;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

public class FullWidthImageView extends NetworkImageViewCustom {

	public FullWidthImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override 
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec){
		Drawable d = getDrawable();

		if(d!=null){
			int width = MeasureSpec.getSize(widthMeasureSpec);
			//proporción forzada a 350x231
			//int height = (int) Math.ceil(((float)width/350.0)*231.0);
			
			//respetando proporción de la imagen 
			int height = (int) Math.ceil((float) width * (float) d.getIntrinsicHeight() / (float) d.getIntrinsicWidth());
			
			setMeasuredDimension(width, height);
		}else{
			super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		}
	}

}
