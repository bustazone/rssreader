package com.bustamante.rssreader.ui.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

import com.bustamante.rssreader.R;
import com.bustamante.rssreader.interfaces.AsyncRequestCallbacks;
import com.bustamante.rssreader.models.RSS_Channel;
import com.bustamante.rssreader.ui.activities.MainActivity;
import com.bustamante.rssreader.ui.adapters.ListRSSItemsAdapter;
import com.bustamante.rssreader.util.serializedData;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

public class NewsListFragment extends android.support.v4.app.Fragment implements OnItemClickListener, OnRefreshListener, AsyncRequestCallbacks {
	
	private View mRootView;
	private TextView channelTitle;
	private TextView channelLink;
	private PullToRefreshListView pullToRefreshView;
	private ListRSSItemsAdapter adapter;
	private Context ctx;
	
	private RSS_Channel mChannel;
	
	private String url;

	public NewsListFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
    	
    	ctx = getActivity();
    	
    	((MainActivity)ctx).ShowSettingOption();
    	
    	mRootView = inflater.inflate(R.layout.news_list_fragment, container, false);
    	channelTitle = (TextView) mRootView.findViewById(R.id.ChannelTitle);
    	channelLink = (TextView) mRootView.findViewById(R.id.ChannelLink);
    	pullToRefreshView = (PullToRefreshListView) mRootView.findViewById(R.id.listViewItems);
        
    	if (mChannel==null || mChannel.getItems()==null)
    	{
    		getData();
    	}
    	
    	fillView();
        
        return mRootView;
    }

    public void fillView () {
		
    	if (mChannel.getTitle()!=null && !mChannel.getTitle().equals(""))
    	{
    		channelTitle.setText(mChannel.getTitle());
    	}
    	else
    	{
    		channelTitle.setText(R.string.default_channel_title);
    	}
    	
    	if (mChannel.getLink()!=null && !mChannel.getLink().equals(""))
    	{
    		channelLink.setVisibility(View.VISIBLE);
    		channelLink.setText(mChannel.getLink());
    	}
    	else
    	{
    		channelLink.setVisibility(View.GONE);
    	}
    	
		adapter = new ListRSSItemsAdapter((MainActivity)ctx, mChannel);
		
		pullToRefreshView.setAdapter(adapter);
				
		pullToRefreshView.setOnRefreshListener(this);
		
		pullToRefreshView.setOnItemClickListener(this);
		
    }
    
	@Override
	public void onSuccess(RSS_Channel channel) {

		mChannel = channel;
		fillView();
		((MainActivity)ctx).HideProgress();
		pullToRefreshView.onRefreshComplete();
		
	}

	@Override
	public void onError(String errorMsg) {

		((MainActivity)ctx).showAlert(getString(R.string.warning), errorMsg);
		((MainActivity)ctx).HideProgress();
		pullToRefreshView.onRefreshComplete();
		
	}

	@Override
	public void onRefresh(PullToRefreshBase refreshView) {

		getData();
		
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		
		((MainActivity)ctx).getSupportFragmentManager().beginTransaction()
	        .replace(R.id.container, new ItemDetailFragment(mChannel, mChannel.getItems().indexOf(parent.getItemAtPosition(position))))
	        .addToBackStack("main")
	        .commit();
		
	}
	
	public void getData ()
	{
		((MainActivity)ctx).ShowProgress("", null);
		
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
		url = sharedPrefs.getString("prefUrlList", null);
		
		if (sharedPrefs.getBoolean("prefUrlPers", false)) {
			url = sharedPrefs.getString("prefUrl", null);
		}
		
		if (url==null || url.equals(""))
		{
			//url = "http://www.xatakandroid.com/tag/feeds/rss2.xml";
	    	url = "https://api.flickr.com/services/feeds/photos_public.gne?format=rss2";
			//url = "http://finofilipino.com/rss";
			//url = "http://google.com/";
		}
		
		mChannel = new serializedData().getChannel(url, ctx, this, true);
	}
	
}
