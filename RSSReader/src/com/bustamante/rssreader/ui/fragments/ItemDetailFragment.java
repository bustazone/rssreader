package com.bustamante.rssreader.ui.fragments;

import com.android.volley.toolbox.NetworkImageView;
import com.bustamante.rssreader.R;
import com.bustamante.rssreader.models.RSS_Channel;
import com.bustamante.rssreader.models.RSS_Item;
import com.bustamante.rssreader.ui.activities.MainActivity;
import com.bustamante.rssreader.ui.controllers.FullWidthImageView;
import com.bustamante.rssreader.ui.imageLoader.NetworkImageViewCustom;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class ItemDetailFragment extends Fragment implements OnClickListener{

	private View mRootView;
	private RSS_Item mItem;
	private RSS_Channel mChannel;
	private Context context;
	
	private Button gotoweb;
	
	public ItemDetailFragment(RSS_Channel channel, int position) {
		mChannel = channel;
		mItem = channel.getItems().get(position);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		context = getActivity();	
		
		((MainActivity)context).HideSettingOption();
		
		mRootView = inflater.inflate(R.layout.item_detail_fragment, container, false);

		NetworkImageViewCustom iv_image = (NetworkImageViewCustom) mRootView.findViewById(R.id.item_image);
		TextView tv_title = (TextView) mRootView.findViewById(R.id.textViewTitle);
		TextView tv_desc = (TextView) mRootView.findViewById(R.id.textViewDesc);
		gotoweb = (Button) mRootView.findViewById(R.id.gotoweb);

    	tv_title.setText(mItem.getTitle());
    	tv_desc.setText(Html.fromHtml(mItem.getDescription()));
        	
    	String imageUrl = null;
    	if (mItem.getImage_url()!=null)
    	{
    		imageUrl = mItem.getImage_url();
    	}
    	else if (mItem.getDescription()!=null)
    	{
    		if (mItem.getDescription().split("img").length>1)
    		{
    			if (mItem.getDescription().split("img")[1].split("src=\"").length>1)
        		{
    				imageUrl = mItem.getDescription().split("img")[1].split("src=\"")[1].split("\"")[0];
        		}
    		}
    	}
    	
    	if (imageUrl==null && mChannel.getImage_url()!=null)
    	{
    		imageUrl = mChannel.getImage_url();
    	}
    		

    	((NetworkImageViewCustom)iv_image).setDefaultImageResId(R.drawable.ic_launcher);
    	((NetworkImageViewCustom)iv_image).setErrorImageResId(R.drawable.ic_launcher);
        if (imageUrl == null){
        	((NetworkImageViewCustom)iv_image).setImageUrl(null, com.bustamante.rssreader.ui.imageLoader.ImageCacheManager.getInstance().getImageLoader());
        }else if (!imageUrl.equals(((NetworkImageViewCustom)iv_image).getTag() == null ? "null" : ((NetworkImageViewCustom)iv_image).getTag().toString())){
        	if (imageUrl != null && imageUrl != "null"){
        		((NetworkImageViewCustom)iv_image).setImageUrl(imageUrl, com.bustamante.rssreader.ui.imageLoader.ImageCacheManager.getInstance().getImageLoader());	
            }
        }else{
        	((NetworkImageViewCustom)iv_image).setImageUrl(null, com.bustamante.rssreader.ui.imageLoader.ImageCacheManager.getInstance().getImageLoader());
        }
        
        if (mItem.getLink()==null && mItem.getLink().equals(""))
        {
        	gotoweb.setVisibility(View.GONE);
        }
        else
        {
        	gotoweb.setOnClickListener(this);
        }
        
		return mRootView;
	}

	@Override
	public void onClick(View v) {

		if (v.equals(gotoweb))
		{			
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mItem.getLink()));
			startActivity(intent);
		}
		
	}
}
