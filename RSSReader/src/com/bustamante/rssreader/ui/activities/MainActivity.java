package com.bustamante.rssreader.ui.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnShowListener;
import android.content.Intent;
import android.graphics.Bitmap.CompressFormat;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.bustamante.rssreader.R;
import com.bustamante.rssreader.ui.fragments.NewsListFragment;
import com.bustamante.rssreader.ui.imageLoader.ImageCacheManager;
import com.bustamante.rssreader.ui.imageLoader.ImageCacheManager.CacheType;
import com.bustamante.rssreader.ui.imageLoader.RequestManager;



public class MainActivity extends FragmentActivity {
	
	NewsListFragment mainfrag;
	
	protected ProgressDialog progressDialog;
	
	public void ShowProgress(String msg, OnShowListener callback) {
		this.progressDialog = new ProgressDialog(this);
		this.progressDialog.setCancelable(false);
		this.progressDialog.setMessage(msg);
		this.progressDialog.setTitle(getString(R.string.wait_dialog_title));
		this.progressDialog.setOnShowListener(callback);
		this.progressDialog.show();		
	}

	public boolean isProgressShowing() {
		if (this.progressDialog != null) {
			return this.progressDialog.isShowing();
		}
		return false;
	}

	public void HideProgress() {
		if (this.progressDialog != null) {
			this.progressDialog.dismiss();
			this.progressDialog.setOnShowListener(null);
		}
	}

	
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		RequestManager.init(this);
		createImageCache();
        setContentView(R.layout.activity_main);
        
        if (savedInstanceState == null) {
        	mainfrag = new NewsListFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, mainfrag)
                    .commit();
        }
    }

    private static int DISK_IMAGECACHE_SIZE = 1024*1024*10;
	private static CompressFormat DISK_IMAGECACHE_COMPRESS_FORMAT = CompressFormat.PNG;
	private static int DISK_IMAGECACHE_QUALITY = 100;  //PNG is lossless so quality is ignored but must be provided
	
    private void createImageCache(){
		ImageCacheManager.getInstance().init(this,
				this.getPackageCodePath()
				, DISK_IMAGECACHE_SIZE
				, DISK_IMAGECACHE_COMPRESS_FORMAT
				, DISK_IMAGECACHE_QUALITY
				, CacheType.DISK);
	}
    
    
    
    private MenuItem mOptionsSettingsMenu;
    		
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        mOptionsSettingsMenu =  menu.findItem(R.id.action_settings);
        return true;
    }

    public static final int RESULT_SETTINGS = 1;
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
        	Intent i = new Intent(this, UserSettingActivity.class);
        	startActivityForResult(i, RESULT_SETTINGS);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    
    public void ShowSettingOption() {
    	if (mOptionsSettingsMenu!=null)
    		mOptionsSettingsMenu.setVisible(true);
	}
	public void HideSettingOption() {
		if (mOptionsSettingsMenu!=null)
			mOptionsSettingsMenu.setVisible(false);
	}
    
    public void showAlert (String title, String msg) {
	    final AlertDialog.Builder builder=new AlertDialog.Builder(this);
	    builder.setTitle(title);
	    builder.setMessage(msg);
	    builder.setIcon(android.R.drawable.ic_dialog_alert);
	    builder.setPositiveButton("OK", new OnClickListener() {
	
		    public void onClick(DialogInterface dialog, int which) {
		    }
		    
	    });
	    builder.show();
    }
   
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
 
        switch (requestCode) {
        case RESULT_SETTINGS:
            if (mainfrag!=null && mainfrag.isAdded())
            {
            	mainfrag.getData();
            }
            break;
 
        }
 
    }
}
