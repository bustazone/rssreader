package com.bustamante.rssreader.ui.adapters;

import java.util.ArrayList;
import java.util.List;

import com.bustamante.rssreader.R;
import com.bustamante.rssreader.models.RSS_Channel;
import com.bustamante.rssreader.models.RSS_Item;
import com.bustamante.rssreader.ui.imageLoader.NetworkImageViewCustom;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class ListRSSItemsAdapter  extends BaseAdapter {

	private Context context;
	private RSS_Channel channel;
	private List<RSS_Item> items;
	
	public ListRSSItemsAdapter(Context mainActivity, RSS_Channel data) {
		this.context = mainActivity;
		this.channel = data;
		if (channel.getItems()==null)
		{
			this.items = new ArrayList<RSS_Item>();
		}
		else
		{
			this.items = channel.getItems();
		}
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
				
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final RSS_Item item = (RSS_Item) items.get(position);
		
		View view = null;
		if (convertView == null) {
            Holder holder = new Holder();
            view = View.inflate(context, R.layout.news_list_fragment_item, null);
            holder.iv_image = (ImageView) view.findViewById(R.id.item_image);
            holder.tv_title = (TextView) view.findViewById(R.id.textViewTitle);
            holder.tv_desc = (TextView) view.findViewById(R.id.textViewDesc);

            view.setTag(holder);
        } else {
            view = convertView;
        }

        Holder holder = (Holder) view.getTag();

    	holder.tv_title.setText(item.getTitle());
    	holder.tv_desc.setText(Html.fromHtml(item.getDescription()));
        	
    	String imageUrl = null;
    	if (item.getImage_url()!=null)
    	{
    		imageUrl = item.getImage_url();
    	}
    	else if (item.getDescription()!=null)
    	{
    		if (item.getDescription().split("img").length>1)
    		{
    			if (item.getDescription().split("img")[1].split("src=\"").length>1)
        		{
    				imageUrl = item.getDescription().split("img")[1].split("src=\"")[1].split("\"")[0];
        		}
    		}
    	}
    	
    	if (imageUrl==null && channel.getImage_url()!=null)
    	{
    		imageUrl = channel.getImage_url();
    	}
    		

    	((NetworkImageViewCustom)holder.iv_image).setDefaultImageResId(R.drawable.ic_launcher);
    	((NetworkImageViewCustom)holder.iv_image).setErrorImageResId(R.drawable.ic_launcher);
        if (imageUrl == null){
        	((NetworkImageViewCustom)holder.iv_image).setImageUrl(null, com.bustamante.rssreader.ui.imageLoader.ImageCacheManager.getInstance().getImageLoader());
        }else if (!imageUrl.equals(((NetworkImageViewCustom)holder.iv_image).getTag() == null ? "null" : ((NetworkImageViewCustom)holder.iv_image).getTag().toString())){
        	if (imageUrl != null && imageUrl != "null"){
        		((NetworkImageViewCustom)holder.iv_image).setImageUrl(imageUrl, com.bustamante.rssreader.ui.imageLoader.ImageCacheManager.getInstance().getImageLoader());	
            }
        }else{
        	((NetworkImageViewCustom)holder.iv_image).setImageUrl(null, com.bustamante.rssreader.ui.imageLoader.ImageCacheManager.getInstance().getImageLoader());
        }
         
        return view;
    }
	
    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
 	   return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    
    class Holder {
    	ImageView iv_image;
        TextView tv_title;
        TextView tv_desc;
    }

}