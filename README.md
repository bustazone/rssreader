#RSSReader#

Lector sencillo de feeds RSS que lista las noticias por fecha y permite acceder al link de internet.


>##Listado##

>Se utiliza un ListView con actualización "PullToRefresh" obtenida gracias a la librería [Android-PullTORefresh](https://github.com/chrisbanes/Android-PullToRefresh) que permite, en este caso, actualizar el listado haciendo swipe hacia abajo con el listado.
>Cada elemento del listado muestra una pequeña representación del feed consistente en un fragmento de título, un fragmento de descripción y una imagen que obtendremos de la manera que se explica más adelante.
>Al hacer click en cada elemento se muestra un detalle de la noticia.


>##Detalle##

>Muestra la misma información que en el elemento del listado pero en formato extendido.
>En esta pantalla la imagen utiliza un controlador personalizado paraque ocupe el máximo ancho de la pantalla sin perder proporciones.
>Además si la noticia tiene un link de acceso a la misma se puede visualizar en el navegador.


>##Imágenes##

>Al no existir una imagen asociada directamente a cada noticia en todos los formatos de RSS, se seguirá la siguiente lista de preferecias para >obtener una imagen ppara mostrar :
>>	1. Etiqueta <enclosure> con type="image/*" para los rss2.
>>	1. El link asociado a la fuente del primer img que se encuentre dentro de la descripción si lo hay.
>>	1. Imagen del canal si existe.
>>	1. Se muestra el robot de android como imagen por defecto.

>Para mostrar estas imágenes que provienen de una url, se utiliza un ImageLoader basado en Volley (sacado de un proyecto personal anterior) para que se descarguen dichas imágenes una sola vez y que se guarde un caché en el dispositivo por si se vuelve a solicitar su descarga.


>##UserSettings##

>Para poder cambiar el origen de los datos se ha creado un pantalla de preferencias donde se ofrecen dos métodos:
>>	1. Seleccionar de una lista predefinida un origen de datos.
>>	1. Introducir otro origen de datos distinto.


>##Obtención de datos##

>Para obtener los datos del feed RSS a través de su url, me he decantado por utilizar el método SAX.
>SAX es un método para el tratamiento de XML. La ventaja de SAX es que parsea el XML línea a línea en vez de partiendo del documento entero. De esta manera se ahorra gran cantidad de tiempo en la petición cuando el XML es accesible a través de una URL.
>El parseo del XML se hace volcando la información en un objeto RRS_Channel que mapea un XML de RSS con datos comunes a todas las versiones de RSS y añadiendo la etiqueta "enclosure", propio de la versión 2 de rss y que guarda una imagen asociada a un elemento del RSS (noticia).

>Para acceder a la información que viene de SAX, se ha creado una tarea asíncrona encargada de llamar en un hilo diferente del principal. 

>Para controlar el regreso de la información, se ha creado un interfaz ("AsyncRequestCallbacks") que define dos callbacks para comunicar la devolución de la tarea asíncrona de SAX con el tratamiento de los datos.

>Para evitar estar solicitando constantemente datos a internet y para permitir cierto funcionamiento offline, se ha introducido unas funciones intermediarias entre el tratamiento de datos dentro del fragment que emite la petición y la tarea asíncrona para permitir que solo solicite a internet si hay conexión, no haya información previamente serializada, o se haya pedido expresamente una actualización de la información serializada que se servirá a la aplicación en otro caso. La intercomunicación entre la tarea asíncrona, el módulo de serialización y los fragments, se hace como se ha dicho antes a través de la interfaz "AsyncRequestCallbacks".